﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Crestron.SimplSharp;

namespace EvertzMMA.SIMPLInterface
{
    /// <summary>
    /// Provides user facing API in SIMPL+ for an Evertz Device's Video output
    /// Requires a Switch Device ID, and an Input Index (1-based) to start.
    /// Registers once DeviceRegistrationComplete is raised.
    /// </summary>
    public class EvertzVideoOutput : EvertzConnector
    {
        public EvertzVideoOutput()
        {
            base.RegisterMonitoredParameter(
                new NotifySubscription { ID = "130", subscriptionCallback = OutuptNameUpdated});

            base.RegisterMonitoredParameter(
                new NotifySubscription { ID = "300", subscriptionCallback = RouteUpdated });

            base.RegisterMonitoredParameter(
                new NotifySubscription { ID = "332", subscriptionCallback = RoutedNameUpdated });
        }

        #region SPlus Delegates
        public OutputNameDelegate OutputName { get; set; }
        public delegate void OutputNameDelegate(SimplSharpString status);

        public CurrentRouteDelegate CurrentRoute { get; set; }
        public delegate void CurrentRouteDelegate(ushort status);

        public CurrentRouteNameDelegate CurrentRouteName { get; set; }
        public delegate void CurrentRouteNameDelegate(SimplSharpString status);
        #endregion

        #region SPlus Methods

        public void setOutputName(SimplSharpString Name)
        {
            if (Name.ToString().Contains('/'))
                throw new ArgumentException("Connector Name cannot contain '/'");

            base.SetEvertzParameter("130", Name.ToString());
        }

        public void setOutputRoute(ushort route)
        {
            if (route < 0)
                return;

            base.SetEvertzParameter("300", route.ToString());
        }

        #endregion

        private void RoutedNameUpdated(DeviceMessageModel DeviceMessage)
        {
            if (OutputName != null && DeviceMessage.Value != null)
                OutputName(DeviceMessage.Value);
        }

        private void RouteUpdated(DeviceMessageModel DeviceMessage)
        {
            if(CurrentRoute != null)
            {
                if (ushort.TryParse(DeviceMessage.Value, out ushort val))
                {
                    CurrentRoute(val);
                }
            }
        }

        private void OutuptNameUpdated(DeviceMessageModel DeviceMessage)
        {
            if (CurrentRouteName != null && DeviceMessage.Value != null)
                CurrentRouteName(DeviceMessage.Value);
        }
    }
}
