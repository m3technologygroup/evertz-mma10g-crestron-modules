﻿using Crestron.SimplSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvertzMMA.SIMPLInterface
{
    public class EvertzAudioOutput : EvertzConnector
    {
        public EvertzAudioOutput()
        {
            base.RegisterMonitoredParameter(
                new NotifySubscription { ID = "200", subscriptionCallback = AudioOutputNameUpdated });

            base.RegisterMonitoredParameter(
                new NotifySubscription { ID = "201", subscriptionCallback = AudioOutputStatusUpdated });

            base.RegisterMonitoredParameter(
                new NotifySubscription { ID = "351", subscriptionCallback = AudioOutputScaleUpdated });

            base.RegisterMonitoredParameter(
                new NotifySubscription { ID = "301", subscriptionCallback = AudioOutputRouteUpdated});

            base.RegisterMonitoredParameter(
                new NotifySubscription { ID = "334", subscriptionCallback = AudioOutputRouteNameUpdated });
        }

        #region SPlus Delegates

        public AudioOutputNameDelegate AudioOutputName { get; set; }
        public delegate void AudioOutputNameDelegate(SimplSharpString data);

        public AudioOutputStatusDelegate AudioOutputStatus { get; set; }
        public delegate void AudioOutputStatusDelegate(ushort data);

        public AudioOutputScaleDelegate AudioOutputScale { get; set; }
        public delegate void AudioOutputScaleDelegate(ushort data);

        public AudioOutputRouteDelegate AudioOutputRoute { get; set; }
        public delegate void AudioOutputRouteDelegate(ushort data);

        public AudioOutputRouteNameDelegate AudioOutputRouteName { get; set; }
        public delegate void AudioOutputRouteNameDelegate(SimplSharpString data);

        #endregion

        public void setOutputName(SimplSharpString Name)
        {
            if (Name.ToString().Contains('/'))
                throw new ArgumentException("Connector Name cannot contain '/'");

            base.SetEvertzParameter("200", Name.ToString());
        }
        public void setOutputScale(ushort value)
        {
            if(value <= 400 && value >=0)
            {
                base.SetEvertzParameter("351", value.ToString());
            }
        }
        public void setOutputRoute(ushort route)
        {
            if (route < 0)
                return;

            base.SetEvertzParameter("301", route.ToString());
        }

        private void AudioOutputRouteNameUpdated(DeviceMessageModel DeviceMessage)
        {
            if (AudioOutputRouteName != null && DeviceMessage.Value != null)
                AudioOutputRouteName(DeviceMessage.Value);
        }

        private void AudioOutputRouteUpdated(DeviceMessageModel DeviceMessage)
        {
            if(AudioOutputRoute != null)
            {
                if (ushort.TryParse(DeviceMessage.Value, out ushort val))
                {
                    AudioOutputRoute(val);
                }
            }
        }

        private void AudioOutputScaleUpdated(DeviceMessageModel DeviceMessage)
        {
            if (AudioOutputScale != null)
            {
                if (ushort.TryParse(DeviceMessage.Value, out ushort val))
                {
                    AudioOutputScale(val);
                }
            }
        }

        private void AudioOutputStatusUpdated(DeviceMessageModel DeviceMessage)
        {
            ushort val = DeviceMessage.Value == "0" ? (ushort)0 : (ushort)1;
            if (AudioOutputStatus != null)
                AudioOutputStatus(val);
        }

        private void AudioOutputNameUpdated(DeviceMessageModel DeviceMessage)
        {
            if (AudioOutputName != null && DeviceMessage.Value != null)
                AudioOutputName(DeviceMessage.Value);
        }
    }
}
