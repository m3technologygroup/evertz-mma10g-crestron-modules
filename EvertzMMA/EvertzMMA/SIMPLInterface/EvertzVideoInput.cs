﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Crestron.SimplSharp;

namespace EvertzMMA
{
    /// <summary>
    /// Provides user facing API in SIMPL+ for an Evertz Device's Video input
    /// Requires a Switch Device ID, and an Input Index (1-based) to start.
    /// Registers once DeviceRegistrationComplete is raised.
    /// </summary>
    public class EvertzVideoInput : EvertzConnector
    {
        public EvertzVideoInput()
        {
            base.RegisterMonitoredParameter(
                new NotifySubscription { ID = "130", subscriptionCallback = InputNameUpdated });

            base.RegisterMonitoredParameter(
                new NotifySubscription { ID = "131", subscriptionCallback = InputStatusUpdated });

            base.RegisterMonitoredParameter(
                new NotifySubscription { ID = "136", subscriptionCallback = InputResolutionUpdated });

            base.RegisterMonitoredParameter(
                new NotifySubscription { ID = "132", subscriptionCallback = AudioStatusUpdated });
        }


        #region SPlusDelegates
        public InputNameDelegate InputName { get; set; }
        public delegate void InputNameDelegate(SimplSharpString status);
        
        public AudioStatusDelegate AudioStatus { get; set; }
        public delegate void AudioStatusDelegate(ushort status);
        
        public InputStatusDelegeate InputStatus { get; set; }
        public delegate void InputStatusDelegeate(ushort status);
        
        public ResolutionStatusDelegate ResolutionStatus { get; set; }
        public delegate void ResolutionStatusDelegate(SimplSharpString status);

        #endregion

        #region SPlusMethods
        public void SetInputName(SimplSharpString Name)
        {
            if (Name.ToString().Contains('/'))
                throw new ArgumentException("Connector Name cannot contain '/'");

            base.SetEvertzParameter("130", Name.ToString());
        }
        #endregion

        private void InputNameUpdated(DeviceMessageModel DeviceMessage)
        {
            if (InputName != null)
                InputName(DeviceMessage.Value);
        }

        private void AudioStatusUpdated(DeviceMessageModel message)
        {
            ushort val = message.Value == "0" ? (ushort)0 : (ushort)1;
            if (AudioStatus != null)
                AudioStatus(val);
        }

        private void InputStatusUpdated(DeviceMessageModel message)
        {
            ushort val = message.Value.ToLower() == "missing" ? (ushort)0 : (ushort)1;
            if (InputStatus != null)
                InputStatus(val);
        }

        private void InputResolutionUpdated(DeviceMessageModel message)
        {
            if (ResolutionStatus != null)
                ResolutionStatus(message.Value);
        }
    }
}
