﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Crestron.SimplSharp;

namespace EvertzMMA.SIMPLInterface
{
    /// <summary>
    /// Provides user facing API in SIMPL+ for an Evertz Device's HDMI output
    /// Extends the monitored parameters of the EvertzVideoOutput
    /// Requires a Switch Device ID, and an Input Index (1-based) to start.
    /// Registers once DeviceRegistrationComplete is raised.
    /// </summary>
    public class EvertzHdmiOutput : EvertzVideoOutput
    {
        public EvertzHdmiOutput()
        {
            base.RegisterMonitoredParameter(
                new NotifySubscription { ID = "434", subscriptionCallback = PuaseStartStatusUpdated });

            base.RegisterMonitoredParameter(
                new NotifySubscription { ID = "166", subscriptionCallback = OutputSinkNameUpdated });

            base.RegisterMonitoredParameter(
                new NotifySubscription { ID = "170", subscriptionCallback = OutputNativeResUpdated });

            base.RegisterMonitoredParameter(
                new NotifySubscription { ID = "167", subscriptionCallback = OutputAudioPresentUpdated });

        }

        #region SPlusDelegates

        public OutputAudioPresentDelegate OutputAudioPresent { get; set; }
        public delegate void OutputAudioPresentDelegate(ushort status);

        public OutputNaitveResDelegate OutputNaitveRes { get; set; }
        public delegate void OutputNaitveResDelegate(SimplSharpString status);

        public OutputSinkNameDelegate OutputSinkName { get; set; }
        public delegate void OutputSinkNameDelegate(SimplSharpString status);

        public OutputPauseStartStatusDelegate OutputPauseStartStatus { get; set; }
        public delegate void OutputPauseStartStatusDelegate(ushort status);
        #endregion

        #region SPlus Methods
        public void PauseOutput() => base.SetEvertzParameter("433", "1");
        public void ResumeOutput() => base.SetEvertzParameter("433", "0");

        public int OutputPuased { get; private set; }
        public void ToggleOutput()
        {
            if (OutputPuased > 0)
                ResumeOutput();
            else
                PauseOutput();
        }

        #endregion


        private void OutputAudioPresentUpdated(DeviceMessageModel DeviceMessage)
        {
            ushort val = DeviceMessage.Value == "0" ? (ushort)0 : (ushort)1;
            if (OutputAudioPresent != null)
                OutputAudioPresent(val);
        }

        private void OutputNativeResUpdated(DeviceMessageModel DeviceMessage)
        {
            if (OutputNaitveRes != null && DeviceMessage.Value != null)
                OutputNaitveRes(DeviceMessage.Value);
        }

        private void OutputSinkNameUpdated(DeviceMessageModel DeviceMessage)
        {
            if (OutputSinkName != null && DeviceMessage.Value != null)
                OutputSinkName(DeviceMessage.Value);
        }

        private void PuaseStartStatusUpdated(DeviceMessageModel DeviceMessage)
        {

            if (ushort.TryParse(DeviceMessage.Value, out ushort val))
            {
                OutputPuased = val;

                if (OutputPauseStartStatus != null)
                    OutputPauseStartStatus(val);

            }
        }
    }
}
