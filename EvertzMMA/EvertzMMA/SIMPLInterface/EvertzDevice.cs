﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Crestron.SimplSharp;
using EvertzMMA.Helpers;

namespace EvertzMMA
{
    /// <summary>
    /// Each evertz device that will be monitored will have an EvertzDevice Instance
    /// Each device has a unique ID and input/output modules are tregistered to the
    /// device module based on the UID. 
    /// The module must be initialized with a device IP address and an API Key.
    /// </summary>
    public class EvertzDevice : IDisposable
    {

        /// <summary>
        /// unique ID used to ascosiate I/O modules with a Device module.
        /// </summary>
        public int DeviceID { get; private set; }
        /// <summary>
        /// IP Address of the registered Device
        /// </summary>
        public string DeviceIP { get; private set; }

        /// <summary>
        /// API Key for the device
        /// </summary>
        private string _APIKey { get; set; }

        /// <summary>
        /// Initialization method for SIMPL+, since S+ does not support passing values into the contructor.
        /// This MUST be invoked before any other methods
        /// </summary>
        /// <param name="IPAddress">Device IP Address, Hostnames NOT supported</param>
        /// <param name="APIKey">Device API Key</param>
        /// <param name="UID">Unique ID used to ascosiate I/O modules with a Device module.</param>
        public void Initialize(string IPAddress, string APIKey, uint UID)
        {
            DeviceID = (int)UID;
            _APIKey = APIKey;

            try
            {
                DeviceIP = Formaters.FormatIP(IPAddress);
            }
            catch (Exception e)
            {
                ErrorLog.Error("Device {0} has invalid IP address. Async Feedback will not work for this device. Details: {1}", DeviceID, e.Message);
            }
            //Register this module
            ModuleRegister.RegisterDeviceModule(this);

            Coms = new DeviceComs(DeviceIP, _APIKey);
            Coms.DeviceOnlineStatusEvent += Coms_DeviceOnlineStatusEvent;

            Subscriber = new SubscriptionSetup(Coms);
            Handeler = new DeviceMessageHandeler(Subscriber);
            Poller = new DevicePoller(Coms, Handeler);

            //now that we are setup, add our base device subscriptions
            RegisterBaseSubscriptions();
        }

        public void Dispose()
        {
            Poller.Dispose();
        }

        #region IO Module Hooks
        /// <summary>
        /// Method for IO modules to bind their event subscriptions.
        /// Events are subscribed to by the Device Message Handeler
        /// </summary>
        /// <param name="subscriptions"></param>
        internal void BindIOModuleSubscription(List<NotifySubscription> subscriptions)
        {
            foreach(NotifySubscription sub in subscriptions)
            {
                Handeler.AddSubscription(sub);
            }
        }

        /// <summary>
        /// Sends a Set command to the device, parses the response to notify all subscribed modules
        /// </summary>
        /// <param name="ID">Parameter ID</param>
        /// <param name="value">new parameter value</param>
        internal void SetEvertzParameter(string ID, string value)
        {
            var result = Coms.setParameter(ID, value.Replace(" ","%20"));
            if (result != null)
                Handeler.HandelMessage(result);
        }

        #endregion

        #region Parameter Subs and delegates for SIMPL+
        public DeviceOnlineStateDelegate DeviceOnlineState { get; set; }
        public delegate void DeviceOnlineStateDelegate(ushort data);

        public ProductNameDelegate ProductName { get; set; }
        public delegate void ProductNameDelegate(SimplSharpString data);

        public SerialNumberDelegate SerialNumber { get; set; }
        public delegate void SerialNumberDelegate(SimplSharpString data);

        public FwVersionDelegate FwVersion { get; set; }
        public delegate void FwVersionDelegate(SimplSharpString data);

        public SystemFaultDelegate SystemFault { get; set; }
        public delegate void SystemFaultDelegate(ushort data);

        private void RegisterBaseSubscriptions()
        {
            //Product Name
            Handeler.AddSubscription(new NotifySubscription { ID = "1", subscriptionCallback = ProductNameUpdated, PreventAsyncSubscribe = true });

            //Serial Number
            Handeler.AddSubscription(new NotifySubscription { ID = "101", subscriptionCallback = SerialNumberUpdated, PreventAsyncSubscribe = true });

            //FW Version
            Handeler.AddSubscription(new NotifySubscription { ID = "103", subscriptionCallback = FirmwareVersionUpdated, PreventAsyncSubscribe = true });

            //Faults Present
            Handeler.AddSubscription(new NotifySubscription { ID = "501", subscriptionCallback = FaultStatusUpdated });
        }

        private void ProductNameUpdated(DeviceMessageModel DeviceMessage)
        {
            if (ProductName != null)
            {
                ProductName(DeviceMessage.Value);
            }
                
        }

        private void FaultStatusUpdated(DeviceMessageModel DeviceMessage)
        {
            if(SystemFault != null)
            {
                if (DeviceMessage.Value == "0")
                    SystemFault(0);
                else
                    SystemFault(1);
            }
        }

        private void FirmwareVersionUpdated(DeviceMessageModel DeviceMessage)
        {
            if (FwVersion != null)
                FwVersion(DeviceMessage.Value);
        }

        private void SerialNumberUpdated(DeviceMessageModel DeviceMessage)
        {
            if (SerialNumber != null)
                SerialNumber(DeviceMessage.Value);
        }

        private void Coms_DeviceOnlineStatusEvent(bool state)
        {
            //Update S+
            if (DeviceOnlineState != null)
                if(state)
                    DeviceOnlineState((ushort)1);
                else
                    DeviceOnlineState((ushort)0);

        }
        #endregion


       

        private DeviceComs Coms { get; set; }
        internal DeviceMessageHandeler Handeler { get; set; }

        internal SubscriptionSetup Subscriber { get; set; }
        private DevicePoller Poller { get; set; }
    }
}
