﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RestSharp;
using Crestron.SimplSharp;

namespace EvertzMMA
{
    /// <summary>
    /// class to handel comunications to and Evertz device via REST API
    /// </summary>
    internal class DeviceComs
    {
        public delegate void DeviceOfflineDelegate(bool state);
        public event DeviceOfflineDelegate DeviceOnlineStatusEvent;
        public bool DeviceOnlineState { get; private set; }

        public DeviceComs(string IPAddress, string APIKey)
        {
            _APIKey = APIKey;

            string BaseURL = string.Format("http://{0}/v.api/apis/EV/", IPAddress);
            _Client = new RestClient(BaseURL);
            _Client.AddDefaultHeader("webeasy-api-key", _APIKey);
            _Client.UserAgent = "Crestron/1.0";
            _Client.Timeout = 5000;
        }

        public List<DeviceMessageModel> setParameter (string[] key, string[] value)
        {
            StringBuilder keys = new StringBuilder("SET/parameters/");
            StringBuilder values = new StringBuilder("/");

            for(int i = 0; i < key.Length; i++)
            {
                if(i > 0)
                {
                    keys.Append(',');
                    values.Append(',');
                }

                keys.Append(key[i]);
                values.Append(value[i]);
            }

            return makeRequest(string.Format("{0}/{1}",keys.ToString(),values.ToString()));
        }

        public List<DeviceMessageModel> setParameter (string key, string value)
        {
            return makeRequest(eRequestType.SET, string.Format("{0}/{1}",key,value));
        }

        public List<DeviceMessageModel> getParameter(string[] param)
        {
            StringBuilder sb = new StringBuilder("GET/parameters/");
            foreach(string p in param)
            {
                if (p != param[0])
                    sb.Append(',');
                sb.Append(string.Format("{0}", p));
            }

            return makeRequest(sb.ToString());
        }
        public List<DeviceMessageModel> getParameter(string param)
        {
            return makeRequest(eRequestType.GET, param);
        }

        public List<DeviceMessageModel> makeRequest(eRequestType type, string param)
        {
            if(type == eRequestType.SUMMARY || type == eRequestType.SERVERSTATUS)
                return makeRequest( type.ToString());

            string paramType = "parameter";
            if (type == eRequestType.SERVERADD || type == eRequestType.SERVERDEL)
                paramType = "server";

            return makeRequest(string.Format("{0}/{1}/{2}", type.ToString(), paramType, param));
        }

        public List<DeviceMessageModel> makeRequest(string URI)
        {
            var request = new RestRequest(URI,DataFormat.Json);
            var response = _Client.Get<List<DeviceMessageModel>>(request);

            updateMissecCommands(response.IsSuccessful);
            if (response.IsSuccessful)
            {
                return response.Data;
            }
            
            return null;
        }

        private string _APIKey { get; set; }
        private RestClient _Client { get; set; }
        private int missedCommands { get; set; }
        

        /// <summary>
        /// Tracks the success state of any command, and if 4 commands come back with error codes, we concider the device to be offline.
        /// we also update the onlinecallback and the deviceonlinestate params here.
        /// </summary>
        /// <param name="IsSuccess"></param>
        private void updateMissecCommands(bool IsSuccess)
        {
            if(IsSuccess)
            {
                missedCommands = 0;

                if (!DeviceOnlineState)
                {
                    DeviceOnlineState = true;

                    try
                    {
                        DeviceOnlineStatusEvent(true);
                    }
                    catch
                    {
                        ErrorLog.Warn("unable to update online state for Evertz Device");
                    }
                }
            }
            else
            {
                missedCommands++;
                if(missedCommands >3 && DeviceOnlineState)
                {
                    DeviceOnlineState = false;

                    try
                    {
                        DeviceOnlineStatusEvent(false);
                    }
                    catch
                    {
                        ErrorLog.Warn("unable to update online state for Evertz Device");
                    }
                }
            }
        }
    }
}
