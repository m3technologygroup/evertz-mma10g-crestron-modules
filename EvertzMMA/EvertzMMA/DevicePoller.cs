﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Crestron.SimplSharp;
using EvertzMMA.Helpers;

namespace EvertzMMA
{
    class DevicePoller : IDisposable
    {
        /// <summary>
        /// Polling interval for the device
        /// </summary>
        const long pollTime = 15000;
        private DeviceComs Coms { get; set; }
        private DeviceMessageHandeler Handeler { get; set; }

        private List<string> Subscriptions { get; set; }

        private CTimer PollTimer { get; set; }
        private CTimer RegTimer { get; set; }
        public DevicePoller(DeviceComs coms, DeviceMessageHandeler handeler)
        {
            Subscriptions = new List<string>();
            Coms = coms;
            Handeler = handeler;
            Handeler.RegisterPoller(this);
            RegTimer = new CTimer(RegTimerCallback, 10000);

            CurrentGroup = 0;

            Coms.DeviceOnlineStatusEvent += Coms_DeviceOnlineStatusEvent;
        }

        public void Dispose()
        {
            if(RegTimer != null)
            {
                RegTimer.Stop();
                RegTimer.Dispose();
            }

            if(PollTimer != null)
            {
                PollTimer.Stop();
                PollTimer.Dispose();
            }
            Subscriptions.Clear();
        }
        /// <summary>
        /// Add a subscription ID to poll
        /// </summary>
        /// <param name="ID">string parameter ID</param>
        public void RegisterSubscription(string ID)
        {
            Subscriptions.Add(ID);
            RegTimer.Reset(5000);
        }

        /// <summary>
        /// Once the device comes online,
        /// we pause the mail poll timer,
        /// and then wait 5 seconds before polling for all parameters
        /// </summary>
        #region Poll Initial Parameters
        public CTimer IntialPollTimer { get; set; }
        //Device has started comunicating, so we nee to poll for all parameters
        private void Coms_DeviceOnlineStatusEvent(bool state)
        {
            if(state)
            {
                IntialPollTimer = new CTimer(InitialPollCallback, 5000);
                PollTimer.Stop();
            }
            
        }

        private void InitialPollCallback(object userSpecific)
        {
            CrestronConsole.PrintLine("getting initial Parameter Set...");
            int group = 0;
            do
            {
                CrestronConsole.PrintLine("Group {0} of {1}", group, Groups);
                var response = Coms.getParameter(GetGroupParams(group));
                if(response != null)
                {
                    group++;
                    string temp;
                    if(Utilities.IsMessageValid(response,out temp))
                        Handeler.HandelMessage(response);
                    else
                    {
                        if(temp != null)
                        {
                            ErrorLog.Warn("Invalid Evertz device parameter {0}, removing from polling...", temp);
                            RemoveBadSubscription(temp);
                        }
                    }
                }

            } while (group < Groups);

            PollTimer.Reset(pollTime); //restart the original poll timer
            IntialPollTimer.Dispose();
            CrestronConsole.PrintLine("Done!");
        }
        #endregion

        /// <summary>
        /// If we encounter an error on a parameter, remove the parameter and reinit
        /// </summary>
        /// <param name="sub"></param>
        private void RemoveBadSubscription(string sub)
        {
            if(Subscriptions.Contains(sub))
            {
                Subscriptions.Remove(sub);
                RegTimerCallback(null);
                InitialPollCallback(null);
            }
        }


        #region Initial Parameter Registrastion
        /// <summary>
        /// This is called 5 sec after the last subscription comes in
        /// </summary>
        /// <param name="userSpecific"></param>
        private void RegTimerCallback(object userSpecific)
        {
            TotalSubs = Subscriptions.Count;
            Groups = TotalSubs / 10;
            if (TotalSubs % 10 == 0)
                Groups--;

            //Start the master poll timer here
            if (PollTimer != null)
                PollTimer.Reset(15000);
            else
                PollTimer = new CTimer(PollTimerCallback, pollTime);
        }



        private int TotalSubs { get; set; }
        private int Groups { get; set; }


        private string[] GetGroupParams(int group)
        {
            int prefix = group * 10;

            List<string> ParamList = new List<string>();

            for (int i = 0; i < 10; i++)
            {
                if (i + prefix >= Subscriptions.Count)
                    break;

                ParamList.Add(Subscriptions[prefix + i]);
            }

            return ParamList.ToArray();
        }
        #endregion


        #region The Actual Poller
        public int CurrentGroup { get; set; }
        private void PollTimerCallback(object userSpecific)
        {
            var response = Coms.getParameter(GetGroupParams(CurrentGroup));
            if (response != null)
            {
                Handeler.HandelMessage(response);

                if (CurrentGroup < Groups)
                    CurrentGroup++;
                else
                    CurrentGroup = 0;
            }

            PollTimer.Reset(pollTime);
        }

        #endregion
    }
}
