﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvertzMMA
{
    internal enum eRequestType
    {
        SUMMARY,
        GET,
        SET,
        SERVERADD,
        SERVERDEL,
        SERVERSTATUS,
        NOTIFYADD,
        NOTIFYDEL
    }
}
