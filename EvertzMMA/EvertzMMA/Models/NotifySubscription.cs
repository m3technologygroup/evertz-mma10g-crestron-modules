﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvertzMMA
{
    internal class NotifySubscription
    {
        public delegate void SubscriptionCallback (DeviceMessageModel DeviceMessage);
        public string ID { get; set; }
        public SubscriptionCallback subscriptionCallback { get; set; }

        /// <summary>
        /// If true, prevents this command from being sent to the ServerAdd list on the device
        /// Paramter will be polled only
        /// </summary>
        public bool? PreventAsyncSubscribe { get; set; }

    }
}
