﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace EvertzMMA
{
    internal class UDPMessageModel
    {
        public string method { get; set; }

        [JsonProperty("params")]
        public IList<UDPParameter> Parameters { get; set; }
    }
    internal class UDPParameter
    {
        public string data { get; set; }
        public string varid { get; set; }
    }
}
