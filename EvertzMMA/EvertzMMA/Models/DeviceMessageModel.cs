﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvertzMMA
{
    internal class DeviceMessageModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public string Status { get; set; }
        public string Error { get; set; }
        public DeviceDataModel Data { get; set; }

    }

    internal class DeviceDataModel
    {
        public IList<DeviceServerModel> Servers { get; set; }
    }

    internal class DeviceServerModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public DeviceNotifyModel Notify { get; set; }
    }
    internal class DeviceNotifyModel
    {
        public string IP { get; set; }
        public IList<string> Parameters { get; set; }
        public int Port { get; set; }
        public string Protocol { get; set; }
    }
}
