﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Crestron.SimplSharp;
using EvertzMMA.Helpers;

namespace EvertzMMA
{
    /// <summary>
    /// Handels the registration of modules based on system ID
    /// </summary>
    static internal class ModuleRegister
    {

        public delegate void DeviceRegistrationCompleteDelegate(object source);
        /// <summary>
        /// Event that is raised once all the device modules have registered.
        /// All I/O modules subscribe and then invoke RegisterIOModule when this event is raised
        /// </summary>
        public static event DeviceRegistrationCompleteDelegate DeviceRegistrationComplete;

        /// <summary>
        /// stores references to all device modules. I/O modules will look up device instance in this list and bind to that instance. 
        /// </summary>
        private static List<EvertzDevice> DeviceList { get; set; }

        //resets each time a device registers, so that we can be sure all of them have come in
        private static CTimer RegTimer { get; set; }

        static ModuleRegister()
        {
            DeviceList = new List<EvertzDevice>();
        }

        /// <summary>
        /// Device Modules call this method to register
        /// </summary>
        /// <param name="device">refference to class registering</param>
        public static void RegisterDeviceModule(EvertzDevice device)
        {
            //Check to make sure the UDP server is registered
            UDPManager.Init();

            //Check for duplicate UIDs
            if (DeviceList.FindIndex(d => d.DeviceID == device.DeviceID) >= 0)
                throw new Exception("Duplicate Device IDs detected. Each Evertz Device module MUST have a unique Device ID.");

            DeviceList.Add(device);

            //if the timer does not exist, create it, if it does exist reset it.
            //this means that we wait 1 sec after the last module registers we raise DeviceRegistrationComplete 
            if (RegTimer == null)
                RegTimer = new CTimer(RegTimerCallback, 1000);
            else
                RegTimer.Reset();
        }
        /// <summary>
        /// IO Modules invoke this once the DeviceRegistrationComplete event is raised
        /// </summary>
        /// <param name="UID">Device ID to bind to</param>
        /// <returns>refference to device class to bind to</returns>
        public static EvertzDevice RegisterIODevice(int UID)
        {
            int idx = DeviceList.FindIndex(d => d.DeviceID == UID);
            if (idx >= 0)
                return DeviceList[idx];
            else
                throw new ArgumentOutOfRangeException(String.Format("Could not register IO module to Device ID {0}, becase this device ID does not exist", UID));
        }

        /// <summary>
        /// Get the instace of the device based on its IP Address
        /// </summary>
        /// <param name="IP">IPv4 Address of the device</param>
        /// <returns>null or EvertzDeviceInstance</returns>
        public static EvertzDevice GetDeviceByIP(string IP)
        {
            if (DeviceList == null || DeviceList.Count == 0)
                return null;

            int idx = DeviceList.FindIndex(d => Formaters.FormatIP(d.DeviceIP) == Formaters.FormatIP(IP));

            if (idx < 0)
                return null;

            return DeviceList[idx];
        }

        private static void RegTimerCallback(object userSpecific)
        {
            //we no longer need this, so remove it
            RegTimer.Dispose();

            //ask all the IO modules to register
            try
            {
                DeviceRegistrationComplete(null);
            }
            catch (NullReferenceException)
            {
                ErrorLog.Warn("There were no Evertz IO modules to register. This would be concidered unusual.");
            }
            catch (Exception e)
            {
                ErrorLog.Error("Could Not register Evertz IO module for unknown casue: {0}, {1}", e.Message, e.StackTrace);
            }
            
        }

       

    }
}
