﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Crestron.SimplSharp;
using Crestron.SimplSharp.CrestronSockets;
using EvertzMMA.Helpers;

namespace EvertzMMA
{
    /// <summary>
    /// Handels the UDP Socket to recieve asynchronous feedback messages from Evertz Devices
    /// </summary>
    static internal class UDPManager
    {
        //Port the server is using for async feedback
        public static int ServerPort { get; private set; }

        public static string ControlSystemIP { get; private set; }

        private static UDPServer server { get; set; }

        /// <summary>
        /// Allows only the first module to open the server.
        /// </summary>
        private static bool isInitialized { get; set; }


        /// <summary>
        /// Init method as we have to wait until SIMPL instantiates module from S+ before we can register a server
        /// Each module will call Init(), use a flag so that it only runs once.
        /// </summary>
        public static void Init()
        {
            if (!isInitialized)
            {
                isInitialized = true;

                CrestronEnvironment.ProgramStatusEventHandler += CrestronEnvironment_ProgramStatusEventHandler;
                //ensure each slot has its own UDP server port
                ServerPort = 54130+(int)InitialParametersClass.ApplicationNumber;
                ControlSystemIP = Formaters.FormatIP(CrestronEthernetHelper.GetEthernetParameter(CrestronEthernetHelper.ETHERNET_PARAMETER_TO_GET.GET_CURRENT_IP_ADDRESS, 0));
                ErrorLog.Notice("Registering Evertz FB Server on Port {0}", ServerPort);
                server = new UDPServer("0.0.0.0",ServerPort, 10240);
                server.EnableUDPServer();
                server.ReceiveDataAsync(ServerCallback);
            }
        }


        /// <summary>
        /// Subscribe to control system program events to dispose of the udp server on program shutdown
        /// </summary>
        private static void CrestronEnvironment_ProgramStatusEventHandler(eProgramStatusEventType programEventType)
        {
            switch (programEventType)
            {
                case eProgramStatusEventType.Stopping:
                    server.DisableUDPServer();
                    server.Dispose();
                    break;
                default:
                    break;
            }
        }

        private static void ServerCallback(UDPServer myUDPServer, int numberOfBytesReceived)
        {
            RouteUDPCallback(myUDPServer.IPAddressLastMessageReceivedFrom, Encoding.UTF8.GetString(myUDPServer.IncomingDataBuffer));

            myUDPServer.ReceiveDataAsync(ServerCallback);
        }

        private static void RouteUDPCallback(string IP, string Data)
        {
            //If the IP is malformed, it will throw an exception, this allows us to disregard it.
            try
            {
                var Device = ModuleRegister.GetDeviceByIP(IP);
                if (Device == null)
                    return;

                Device.Handeler.HandleUDPMessage(Data);
            }
            catch
            {
                return;
            }
        }
    }
}
