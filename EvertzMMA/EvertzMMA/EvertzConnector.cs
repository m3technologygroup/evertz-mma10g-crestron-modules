﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Crestron.SimplSharp;

namespace EvertzMMA
{
    public class EvertzConnector
    {
        public int SwitchDeviceID { get; private set; }
        public int ConnectorIndex { get; private set; }

        protected EvertzDevice BaseDevice { get; set; }

        /// <summary>
        /// List of subscriptions.
        /// The base class initializes it, and the child class loads it.
        /// Upon registration, the registration process will ask for this list.
        /// </summary>
        private List<NotifySubscription> Subscriptions { get; set; }

        /// <summary>
        /// We will get subscription IDs before we know what port this module is connected to #ThanksCrestron
        /// Becase of that we need to cache parameters without the port ID, and then append it once S+ calls the init method.
        /// </summary>
        private List<NotifySubscription> QueuedSubscriptions { get; set; }
        public EvertzConnector()
        {
            Subscriptions = new List<NotifySubscription>();
            QueuedSubscriptions = new List<NotifySubscription>();

            //0 is a valid index, so initialize this to -1 so that we know when S+ has initilized the module
            ConnectorIndex = -1;
        }


        public void Init(uint switchDeviceID, uint connectorIndex)
        {
            SwitchDeviceID = (int)switchDeviceID;
            ConnectorIndex = (int)connectorIndex-1; //User will enter as 1- based, API is 0 Based

            //subscribe to the event to register this IO module
            ModuleRegister.DeviceRegistrationComplete += ModuleRegister_DeviceRegistrationComplete;

            //now that we have the conenctor ID, append the conenctor ID to all queued subs
            foreach(NotifySubscription sub in QueuedSubscriptions)
            {
                RegisterMonitoredParameter(sub);
            }
        }

        //child module calls this to register parameters to monitor, all parameters must eb registered before 
        internal void RegisterMonitoredParameter(NotifySubscription subscription) 
        {
            if(ConnectorIndex >= 0)
            {
                Subscriptions.Add(new NotifySubscription
                {
                    subscriptionCallback = subscription.subscriptionCallback,
                    ID = string.Format("{0}.{1}", subscription.ID, ConnectorIndex)
                });
            }
            else
            {
                //since we doin't know the port ID yet, just save it and we will append it later.
                QueuedSubscriptions.Add(subscription);
            }
        }

        /// <summary>
        /// Set a Parameter on this connector
        /// </summary>
        /// <param name="param">Parameter ID, Connector index will be appended automagically</param>
        /// <param name="value">new parameter value</param>
        internal void SetEvertzParameter(string ID, string value)
        {
            BaseDevice.SetEvertzParameter(string.Format("{0}.{1}", ID, ConnectorIndex), value);
        }

        private void ModuleRegister_DeviceRegistrationComplete(object source)
        {
            
            BaseDevice = ModuleRegister.RegisterIODevice(SwitchDeviceID);
            BaseDevice.BindIOModuleSubscription(Subscriptions);
        }
    }
}
