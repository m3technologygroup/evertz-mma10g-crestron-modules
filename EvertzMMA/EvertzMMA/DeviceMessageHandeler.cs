﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Crestron.SimplSharp;
using Newtonsoft.Json;
using EvertzMMA.Helpers;

namespace EvertzMMA
{

    class DeviceMessageHandeler
    {
        public SubscriptionSetup Subscriber { get; set; }

        private DevicePoller Poller { get; set; }

        private List<NotifySubscription> Subscriptions { get; set; }

        public DeviceMessageHandeler(SubscriptionSetup subscriber)
        {
            Subscriptions = new List<NotifySubscription>();
            Subscriber = subscriber;

            LastSentMessages = new List<DeviceMessageModel>();
        }

        public void AddSubscription(NotifySubscription subscription)
        {
            Subscriptions.Add(subscription);

            //If the parameter is set to not register this for async FB, then do not send to the Subscriber
            if(subscription.PreventAsyncSubscribe == null || subscription.PreventAsyncSubscribe == false)
                Subscriber.AddSubscription(subscription.ID);

            if (Poller != null)
                Poller.RegisterSubscription(subscription.ID);
        }

        //Method that handles an API request and routes fb to all subscribed handelers
        public void HandelMessage (List<DeviceMessageModel> messages)
        {
            if (messages != null)
                foreach (DeviceMessageModel message in messages)
                {
                    if(message.ID != null && message.Value != null)
                        RouteMessage(message);
                }
                    
        }

        public void HandleUDPMessage(string Message)
        {
            try
            {
                UDPMessageModel message = JsonConvert.DeserializeObject<UDPMessageModel>(Message);
                HandelMessage(TransformUdpMessageToDeviceMessgae(message));
            }
            catch
            {
                ErrorLog.Notice("Recieved malformated Json status from Evertz Device");
            }
        }

        public void RegisterPoller(DevicePoller poller) => Poller = poller;

        private List<DeviceMessageModel> TransformUdpMessageToDeviceMessgae(UDPMessageModel message)
        {
            List<DeviceMessageModel> deviceMessages = new List<DeviceMessageModel>();

            if (message.Parameters == null || message.Parameters.Count == 0)
                return null;

            foreach (UDPParameter param in message.Parameters)
            {
                deviceMessages.Add(new DeviceMessageModel { ID = param.varid, Value = param.data });
            }

            return deviceMessages;
        }

        //takes a message and invlokes any registered callbacks that match the message
        private void RouteMessage(DeviceMessageModel message)
        {
            if (Subscriptions == null || Subscriptions.Count == 0)
                return;

            //if the data is not new, exit
            if (!CheckForNewData(message))
                return;

            foreach (var sub in Subscriptions)
            {
                if(sub.ID == Formaters.RemoveEvertzType(message.ID))
                {
                    if (sub.subscriptionCallback != null)
                        sub.subscriptionCallback(message);
                }
            }
        }


        #region State Deduplication
        /// <summary>
        /// Check to see if the content of message is different from the last time this methos was called
        /// checks based on ID, if a value for an ID is new, returns true, otherwise false.
        /// Currently only checks the "Value" field for data
        /// </summary>
        /// <param name="message">DeviceMessage</param>
        /// <returns>boolean: Is New Data</returns>
        private bool CheckForNewData(DeviceMessageModel message)
        {
            //search for an index
            int idx = LastSentMessages.FindIndex(m => Formaters.RemoveEvertzType(m.ID) == Formaters.RemoveEvertzType(message.ID));

            //if the ID is new to us, store it, and return true
            if(idx == -1)
            {
                LastSentMessages.Add(message);
                return true;
            }

            if(LastSentMessages[idx].Value != message.Value)
            {
                LastSentMessages[idx].Value = message.Value;
                return true;
            }
            return false;
        }
        //stores the last message that came in for each ID
        private List<DeviceMessageModel> LastSentMessages { get; set; }
        #endregion
    }
}
