﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvertzMMA.Helpers
{
    internal class Utilities
    {

        /// <summary>
        /// Looks at a device message to determine if there is a bad parameter that shoudl be removed from polling
        /// if there are errors, it will return false, if there are errors, it will return true and expose the error parameter ID as errorParameter
        /// </summary>
        /// <param name="message">DeviceMessage to check</param>
        /// <param name="errorParameter">if there is an error ID, is sent here</param>
        /// <returns></returns>
        public static bool IsMessageValid(DeviceMessageModel message, out string errorParameter)
        {
            if (message.Error == null && message.ID != null)
            {
                errorParameter = null;
                return true;
            }

            string outTemp = null;

            if(message.Error != null)
            {

                if(message.Error.Contains("indescribable parameter -"))
                {
                    int start = message.Error.LastIndexOf('-');
                    outTemp = message.Error.Substring(start + 1).Replace(" ", "");
                }
            }

            errorParameter = outTemp;
            return false;
        }

        public static bool IsMessageValid(List<DeviceMessageModel> message, out string errorParameter)
        {
            string parameter = null;
            bool returnVal = true;

            foreach(var msg in message)
            {
                string tmp = null;
                if(!IsMessageValid(msg,out tmp))
                {
                    returnVal = false;
                    parameter = tmp;
                    break;
                }
            }

            errorParameter = parameter;
            return returnVal;
        }
    }
}
