﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvertzMMA.Helpers
{
    static class Formaters
    {
        public static string RemoveEvertzType(string input)
        {
            int idx = input.IndexOf('@');
            return input.Substring(0, idx);
        }
        public static string FormatIP(string input)
        {
            var octets = input.Split('.');
            if (octets.Length != 4)
                throw new ArgumentException(string.Format("{0} is not formated like an IPv4 Address", input));

            int[] intOctets = new int[4];

            for (int i = 0; i < 4; i++)
            {
                try
                {
                    intOctets[i] = Int32.Parse(octets[i]);
                }
                catch (Exception)
                {
                    throw new ArgumentException(string.Format("{0} contains characters other than numbers and '.'", input));
                }

                if (intOctets[i] > 255 || intOctets[i] < 0)
                {
                    throw new ArgumentOutOfRangeException("Each octet must be between 0 and 255");
                }
            }

            return string.Format("{0}.{1}.{2}.{3}", intOctets[0], intOctets[1], intOctets[2], intOctets[3]);
        }

    }
}
