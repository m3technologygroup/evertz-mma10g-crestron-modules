﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Crestron.SimplSharp;
using EvertzMMA.Helpers;

namespace EvertzMMA
{
    internal class SubscriptionSetup
    {
        public int ServerID { get; private set; }

        //store the instance of the coms client for use in here
        private DeviceComs Coms { get; set; }
        public bool ServerRegistered { get; private set; }
        /// <summary>
        /// holds a list of subscriptions that are currently active on the device
        /// </summary>
        private List<string> SubscriptionList { get; set; }
        /// <summary>
        /// List of subscriptions that need to be added to the device when it comes online
        /// </summary>
        private List<string> SubscriptionQueue { get; set; }

        /// <summary>
        /// Name of the server, auto generates as 'crestron' plus the last 6 of the primay MAC address
        /// EG: 'crestronA0B1C2'
        /// </summary>
        private string ServerName { get; set; }


        public SubscriptionSetup(DeviceComs coms)
        {
            Coms = coms;
            SubscriptionList = new List<string>();
            SubscriptionQueue = new List<string>();
            Coms.DeviceOnlineStatusEvent += Coms_DeviceOnlineStatusEvent;

            //build the server name
            string MACAddress = CrestronEthernetHelper.GetEthernetParameter(CrestronEthernetHelper.ETHERNET_PARAMETER_TO_GET.GET_MAC_ADDRESS, 0);
            MACAddress =  Regex.Replace(MACAddress, "[^a-zA-Z0-9]", String.Empty).ToUpper();
            ServerName = string.Format("crestron{0}",MACAddress.Substring(6,6));

            //in the unlikley situation where the device is already online, send the subscription add now
            if (Coms.DeviceOnlineState)
                SetupNotifyServer();
        }

        /// <summary>
        /// Public method to add a parameter should subscribes to
        /// </summary>
        /// <param name="parameter"></param>
        public void AddSubscription(string parameter)
        {
            //if the server is already online, just dedupe and send
            if(ServerID > 0)
            {
                registerQueuedSubscriptions(CheckSubscriptions(new List<string> { parameter }));
            }
            else
            {
                SubscriptionQueue.Add(parameter);
            }
        }


        //once it comes online, lets subscribe
        private void Coms_DeviceOnlineStatusEvent(bool state)
        {
            if (state)
                SetupNotifyServer();
        }

        //all requets are blocking calls, so step by step setup the subscription
        private void SetupNotifyServer()
        {
            Tuple<int, List<string>> serverInfo = null;
            try
            {
                serverInfo = GetServerStatus();
            }
            catch (Exception e)
            {
                ErrorLog.Error("Could not register async feedback for Evertz device due to {0}", e.Message);
                return;
            }

            //load the ID and the registered parameter lsit
            ServerID = serverInfo.Item1;
            SubscriptionList = serverInfo.Item2;

            //if there are no subscriptions to send, we are done and successful
            if (SubscriptionQueue.Count == 0)
                return;

            //get list of queued subscriptions and dedupe them
            List<string> itemsToSubscribeTo = CheckSubscriptions(SubscriptionQueue);

            //request subscriptions that are not already subscribed
            if(itemsToSubscribeTo.Count > 0)
            {
                registerQueuedSubscriptions(itemsToSubscribeTo);
            }
        }

        /// <summary>
        /// Check this list of currently subscribed parameters and return a list of new parameters that are not active yet
        /// </summary>
        /// <param name="subscriptions"></param>
        /// <returns></returns>
        private List<string> CheckSubscriptions(List<string> subscriptions)
        {
            List<string> SubsToAdd = new List<string>();

            foreach(string sub in subscriptions)
            {
                if (SubscriptionList.FindIndex(s => s == sub) < 0)
                    SubsToAdd.Add(sub);
            }

            return SubsToAdd;
        }

        /// <summary>
        /// Looks at the devices servers table to figure out if this CS is already connected.
        /// </summary>
        /// <returns>ServerID, List of registered parameters</returns>
        private Tuple<int, List<string>> GetServerStatus()
        {
            var ServerStatus = Coms.makeRequest(eRequestType.SERVERSTATUS,"");

            int idx = -1;
            //if we get a null back or an error back, there there are no servers
            if(ServerStatus != null)
            {
                idx = GetServerIndex(ServerStatus[0].Data);
            }
            

            //server does not exist, so we need to add it, then fetch the data
            if(idx == -1)
            {
                RegisterServer();
                return GetServerStatus();
            }
            
            //server exists but has a different IP than this CS, soremove it and start again
            if(Formaters.FormatIP(ServerStatus[0].Data.Servers[idx].Notify.IP) != UDPManager.ControlSystemIP)
            {
                RemoveServer(ServerStatus[0].Data.Servers[idx].ID);
                return GetServerStatus();
            }

            //at this point we should have a valid index for a server that is pointed at this CS
            List<string> ParameterList = new List<string>();
            foreach(string param in ServerStatus[0].Data.Servers[idx].Notify.Parameters)
            {
                ParameterList.Add(Formaters.RemoveEvertzType(param));
            }

            return Tuple.Create(ServerStatus[0].Data.Servers[idx].ID, ParameterList);
        }

        /// <summary>
        /// Finds first index containing server entry with matching server name
        /// </summary>
        /// <param name="data">data returned by SERVERSTATUS</param>
        /// <returns>index of iList<servers> with matching name</servers></returns>
        private int GetServerIndex(DeviceDataModel data)
        {
            //iList does not have findindex method, so manually find the matching index of out server name
            int idx = -1;

            if (data != null && data.Servers.Count > 0)
            {
                for (int i = 0; i < data.Servers.Count; i++)
                {
                    if (data.Servers[i].Name == ServerName)
                        idx = i;
                }
            }

            return idx;
        }

        private bool RegisterServer()
        {
            var Response = Coms.makeRequest(eRequestType.SERVERADD, string.Format("{0}/{1}/{2}/udp",UDPManager.ControlSystemIP,ServerName,UDPManager.ServerPort));
            if (Response[0].Status == "success" || Response[0].Error == "error to register server- Server exists..")
                return true;
            throw new Exception("Error adding server to Evertz device. async feedback will not work.");
        }

        private bool RemoveServer(int serverID)
        {
            var Response = Coms.makeRequest(eRequestType.SERVERDEL, serverID.ToString());
            if (Response[0].Status == "success" || Response[0].Error == "error to delete  registered server - Server not exists..")
                return true;
            ErrorLog.Warn("could not remove server from Evertz device, duplicte server entries may exist");
            return false;
        }

        private void registerQueuedSubscriptions(List<string> subscriptions)
        {
            if (subscriptions.Count == 0)
                return;

            foreach(string sub in subscriptions)
            {
                //subscribe, and if sucessful, add it to the list of actibe subscriptions
                if (SendSubscribe(sub))
                    SubscriptionList.Add(sub);
            }
        }

        private bool SendSubscribe(string parameter)
        {
            var response = Coms.makeRequest(eRequestType.NOTIFYADD, string.Format("{0}/{1}",ServerID,parameter));
            if (response[0].Status != null)
                return true;

            return false;
        }
    }
}
